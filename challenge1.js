//====================================================================================nomor 1
function changeWord (selectedText, changedText, text){    
    const ubah = text.replace(selectedText,changedText)
    return ubah
}

const kalimat1 ='Andini sangat mencintai kamu selamanya'
const kalimat2 ='Gunung bromo tak akan mampu menggambarkan betapa luasnya cintaku padamu'

console.log("Nomor 1");
console.log(changeWord('mencintai','membenci',kalimat1));
console.log(changeWord('bromo','semeru',kalimat2));

//====================================================================================nomor 2
const checkTypeNumber = givenNumber => {
    if (!givenNumber) {
      return "Error: Bro where is the parameter?";
    }
    if (typeof givenNumber == "number") {
      if (givenNumber % 2 == 0) {
        return "GENAP";
      } else {
        return "GANJIL";
      }
    } else {
      return "Error: invalid data type";
    }
  }
  

  console.log("Nomor 2");
  console.log(checkTypeNumber(10));
  console.log(checkTypeNumber(3));
  console.log(checkTypeNumber("3")); 
  console.log(checkTypeNumber({}));
  console.log(checkTypeNumber([]));
  console.log(checkTypeNumber()); 

//====================================================================================nomor 3
function checkEmail(email) {
    const pattern = /^[a-zA-Z0-9]+@[a-zA-Z.]+.\.[a-zA-Z]{2,6}$/;
    if (pattern.test(email) && typeof email === "string") {
      return "VALID";
    } else {
      return "INVALID"
    }
  }

  console.log("Nomor 3");
  console.log(checkEmail("apranata@binar.co.id"));
  console.log(checkEmail("apranata@binar.com"));
  console.log(checkEmail("apranata@binar"));
  console.log(checkEmail("apranata"));
  console.log(checkEmail(3322));
  console.log(checkEmail()); //====================================================================================nomor 4
  function isValidPassword(givenPassword) {
    const passwordFormat = /(?=.*[a-z])(?=.*[0-9])(?=.*[A-Z])(?=.{8})/;
    if(typeof givenPassword === "string"){
      if (passwordFormat.test(givenPassword)) {
        return true;
      } else {
        return false;
      }
    }else{
      return givenPassword === undefined ? "Error: Password harus diisi!" : "Error : invalid data type"
    }
  }

  console.log("Nomor 4");
  console.log(isValidPassword("JohnLennon1940"));
  console.log(isValidPassword("johnlennon1940"));
  console.log(isValidPassword("@johnlennon"));
  console.log(isValidPassword("John19"));
  console.log(isValidPassword(0));
  console.log(isValidPassword());

//====================================================================================nomor 5
function getSplitName(personName) {
    if (typeof personName === "string") {
      const name = personName.split(" ");
      if (name.length <= 3) {
        return {
          firstName: name[0],
          middleName: name.length === 3 ? name[1] : null ,
          lastName: name.length === 2 ? name[1] : name[2] || null,
        };
      } else {
        return "Error: This function is only for 3 character name";
      }
    } else {
      return "Error: invalid data type";
    }
  }
  
  console.log("Nomor 5");
  console.log(getSplitName("Hanif Nugroho Jati"));
  console.log(getSplitName("John Lennon"));
  console.log(getSplitName("Einstein"));
  console.log(getSplitName("Aurora Aureliya Sukma Darma"));
  console.log(getSplitName(0));

//====================================================================================nomor 6
function getAngkaTerbesarKedua(dataNumbers) {
  if (dataNumbers !== 0 && !dataNumbers ){
    return "error karena data null"
    } else if (dataNumbers === 0) {
    return 'Error data nol'
    } else {
      dataNumbers.sort(function(a , b){
      return a - b
    }).reverse()

    let angkaTerbesar = Math.max(...dataNumbers)
    let filterAngka = dataNumbers.filter(angka => angka == angkaTerbesar).length
    let terbesarKedua = 0
    let angkaTerbesarKedua = dataNumbers[filterAngka - terbesarKedua]
    return angkaTerbesarKedua;
    }
  }
  
  const dataNumbers = [9, 4, 7, 7, 4, 3, 2, 2, 8];
  console.log("Nomor 6");
  console.log(getAngkaTerbesarKedua(dataNumbers));
  console.log(getAngkaTerbesarKedua(0));
  console.log(getAngkaTerbesarKedua());

//====================================================================================nomor 7
const dataPenjualanPakAldi = [
    {
      namaProduct: 'Sepatu Futsal Nike Vapor Academy 8',
      hargaSatuan: 760000,
      kategori: 'Sepatu Sport',
      totalTerjual: 90,
    },
    {
      namaProduct: 'Sepatu Warrior Tristan Black Brown High',
      hargaSatuan: 960000,
      kategori: 'Sepatu Sneaker',
      totalTerjual: 37,
    },
    {
      namaProduct: 'Sepatu Warrior Tristan Maroon High ',
      kategori: 'Sepatu Sneaker',
      hargaSatuan: 360000,
      totalTerjual: 90,
    },
    {
      namaProduct: 'Sepatu Warrior Rainbow Tosca Corduroy',
      hargaSatuan: 120000,
      kategori: 'Sepatu Sneaker',
      totalTerjual: 90,
    },
  ];
  
  function getTotalPenjualan(dataPenjualan) {
    if (Array.isArray(dataPenjualan)) {
      let total = 0;
      for (let i = 0; i < dataPenjualan.length; i++) {
        total += dataPenjualan[i].totalTerjual;
      }
      return total;
    } else {
      return 'Error : invalid data type';
    }
  }

  console.log("Nomor 7");
  console.log(getTotalPenjualan(dataPenjualanPakAldi));

  //====================================================Nomor8
  const dataPenjualanNovel = [
    {
      idProduct: 'BOOK002421',
      namaProduk: 'Pulang - Pergi',
      penulis: 'Tere Liye',
      hargaBeli: 60000,
      hargaJual: 86000,
      totalTerjual: 150,
      sisaStok: 17,
    },
    {
      idProduct: 'BOOK002351',
      namaProduk: 'Selamat Tinggal',
      penulis: 'Tere Liye',
      hargaBeli: 75000,
      hargaJual: 103000,
      totalTerjual: 171,
      sisaStok: 20,
    },
    {
      idProduct: 'BOOK002941',
      namaProduk: 'Garis Waktu',
      penulis: 'Fiersa Besari',
      hargaBeli: 67000,
      hargaJual: 99000,
      totalTerjual: 213,
      sisaStok: 5,
    },
    {
      idProduct: 'BOOK002941',
      namaProduk: 'Laskar Pelangi',
      penulis: 'Andrea Hirata',
      hargaBeli: 55000,
      hargaJual: 68000,
      totalTerjual: 20,
      sisaStok: 56,
    },
  ];
  
  function sumSimilarKey(dataPenjualan) {
    let holder = {};
    dataPenjualan.forEach(function (dPenjualan) {
      if (holder.hasOwnProperty(dPenjualan.penulis)) {
        holder[dPenjualan.penulis] += dPenjualan.totalTerjual;
      } else {
        holder[dPenjualan.penulis] = dPenjualan.totalTerjual;
      }
    });
    let obj2 = [];
    for (let prop in holder) {
      obj2.push({ penulis: prop, totalTerjual: holder[prop] });
    }
    return obj2;
  }
  
  
  function getInfoPenjualan(dataPenjualan) {
    if (Array.isArray(dataPenjualan)) {
      let totalKeuntungan = 0;
      let totalModal = 0;

      for (let i = 0; i < dataPenjualan.length; i++) {
        totalKeuntungan += (dataPenjualan[i].hargaJual - dataPenjualan[i].hargaBeli) * dataPenjualan[i].totalTerjual; 
        totalModal += dataPenjualan[i].hargaBeli * (dataPenjualan[i].sisaStok + dataPenjualan[i].totalTerjual);
      }
  
      const totalTerjualBerdasarkanPenulis = sumSimilarKey(dataPenjualan);

      const max = dataPenjualan.reduce(function (prev, cur) {
        return prev.totalTerjual > cur.totalTerjual ? prev : cur;
      });
      const maxPenulis = totalTerjualBerdasarkanPenulis.reduce(function (prev, cur) {
        return prev.totalTerjual > cur.totalTerjual ? prev : cur;
      });
  
      return {
        totalKeuntungan:totalKeuntungan.toLocaleString("id-ID", {style: "currency", currency: "IDR" }),
        totalModal:totalModal.toLocaleString("id-ID", {style: "currency", currency: "IDR" }),
        persentaseKeuntungan: `${(totalKeuntungan / totalModal * 100).toFixed()}%`,
        produkBukuTerlaris: max.namaProduk,
        penulisTerlaris: maxPenulis.penulis,
      };
    } else {
      return 'Error : invalid data type';
    }
  }
  

  console.log("Nomor 8");
  console.log(getInfoPenjualan(dataPenjualanNovel));